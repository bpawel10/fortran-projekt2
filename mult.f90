module mult
    implicit none
contains
    subroutine mm(first, second, multiply, status)
        real (kind = 8), intent(in) :: first(:,:) ! pierwsza macierz
        real (kind = 8), intent(in) :: second(:,:) ! druga macierz
        real (kind = 8), intent(out) :: multiply(:,:) ! macierz wynikowa
        integer (kind = 4), intent(out) :: status ! kod błędu, 0 gdy OK
        integer (kind = 8) :: i, j, k
        real (kind = 8) :: tmp

        if (size(first(1,:)) /= size(second(:,1))) then
            status = 1
            return
        end if

        if (size(first(1,:)) /= size(multiply(1,:)) .or. size(second(:,1)) /= size(multiply(:,1))) then
            status = 2
            return
        end if

        do j=1,size(second(1,:))
            do i=1,size(first(:,1))
                tmp = 0.0
                do k=1,size(first(1,:))
                    tmp = tmp + first(i,k) * second(k,j)
                enddo
                multiply(i,j) = tmp
            enddo
        enddo

        status = 0
    end subroutine

    subroutine mm2(first, second, multiply, status)
        real (kind = 8), intent(in) :: first(:,:) ! pierwsza macierz
        real (kind = 8), intent(in) :: second(:,:) ! druga macierz
        real (kind = 8), intent(out) :: multiply(:,:) ! macierz wynikowa
        integer (kind = 4), intent(out) :: status ! kod błędu, 0 gdy OK
        integer (kind = 8) :: i, j, k

        if (size(first(1,:)) /= size(second(:,1))) then
            status = 1
            return
        end if

        if (size(first(1,:)) /= size(multiply(1,:)) .or. size(second(:,1)) /= size(multiply(:,1))) then
            status = 2
            return
        end if

        do j=1,size(second(1,:))
            do i=1,size(first(:,1))
                multiply(i,j) = dot_product(first(i,:), second(:,j))
            enddo
        enddo

        status = 0
    end subroutine

    subroutine mm3(first, second, multiply, status)
        real (kind = 8), intent(in) :: first(:,:) ! pierwsza macierz
        real (kind = 8), intent(in) :: second(:,:) ! druga macierz
        real (kind = 8), intent(out) :: multiply(:,:) ! macierz wynikowa
        integer (kind = 4), intent(out) :: status ! kod błędu, 0 gdy OK
        integer (kind = 8) :: i, j, k, ii, jj
        real (kind = 8) :: tmp
        integer (kind = 4) :: ichunk

        ichunk = 512

        if (size(first(1,:)) /= size(second(:,1))) then
            status = 1
            return
        end if

        if (size(first(1,:)) /= size(multiply(1,:)) .or. size(second(:,1)) /= size(multiply(:,1))) then
            status = 2
            return
        end if

        do jj=1,size(second(1,:)), ichunk
            do ii=1,size(first(:,1)), ichunk
                do j=jj, min(jj+ichunk-1, size(second(1,:)))
                    do i=ii, min(ii+ichunk-1, size(first(:,1)))
                        tmp = 0.0
                        do k=1,size(first(1,:)), ichunk
                            tmp = tmp + first(i,k) * second(k,j)
                        enddo
                        multiply(i,j) = tmp
                    enddo
                enddo
            enddo
        enddo

        status = 0
    end subroutine

    subroutine mm4(first, second, multiply, status)
        real (kind = 8), intent(in) :: first(:,:) ! pierwsza macierz
        real (kind = 8), intent(in) :: second(:,:) ! druga macierz
        real (kind = 8), intent(out) :: multiply(:,:) ! macierz wynikowa
        integer (kind = 4), intent(out) :: status ! kod błędu, 0 gdy OK
        integer (kind = 8) :: i, j, k, ii, jj
        integer (kind = 4) :: ichunk

        ichunk = 512

        if (size(first(1,:)) /= size(second(:,1))) then
            status = 1
            return
        end if

        if (size(first(1,:)) /= size(multiply(1,:)) .or. size(second(:,1)) /= size(multiply(:,1))) then
            status = 2
            return
        end if

        do jj=1,size(second(1,:)), ichunk
            do ii=1,size(first(:,1)), ichunk
                do j=jj, min(jj+ichunk-1, size(second(1,:)))
                    do i=ii, min(ii+ichunk-1, size(first(:,1)))
                        multiply(i,j) = dot_product(first(i,:), second(:,j))
                    enddo
                enddo
            enddo
        enddo

        status = 0
    end subroutine
end module
