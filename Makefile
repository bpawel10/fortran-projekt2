PFUNIT = /opt/pfunit/pfunit-serial
F90_VENDOR = Intel

ifneq ($(BASEMK_INCLUDED),YES)
include $(PFUNIT)/include/base.mk
endif

FC = gfortran
LIBS = $(PFUNIT)/lib/libpfunit$(LIB_EXT)
FLAGS=-O2 -ffree-form -std=f2008 -fimplicit-none -Wall -pedantic -fbounds-check -cpp -funroll-loops
SRCS = $(wildcard *.pf)
OBJS = $(SRCS:.pf=.o)

FLAGS += -I$(SRC_DIR) -I$(PFUNIT)/mod

%.F90: %.pf
	$(PFUNIT)/bin/pFUnitParser.py $<  $@

%.o: %.F90
	$(FC) -c $(FLAGS) $<

test: testSuites.inc ./mult.o $(OBJS)
	$(F90) -o ../bin/$@ -I$(PFUNIT)/mod -I$(PFUNIT)/include -I../src \
		$(PFUNIT)/include/driver.F90 \
		./*.o $(LIBS) $(FLAGS)

clean: rm *.o *.mod
